//
//  MasterViewController.m
//  Presenting Master-Detail Views with UISplitViewController
//
//  Created by Vandad NP on 02/07/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "FamousPerson.h"

@interface MasterViewController ()
{

}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
	if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
	{
		self.clearsSelectionOnViewWillAppear = NO;
		self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
	}

	[super awakeFromNib];
}

- (void)viewDidLoad
{
	[super viewDidLoad];

	
	self.detailViewController = (DetailViewController *) [self.splitViewController.viewControllers.lastObject topViewController];

	UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
	self.navigationItem.rightBarButtonItem = addButton;
	self.navigationItem.leftBarButtonItem = self.editButtonItem;
	self.title = @"Famous People";

	self.people = [FamousPerson MR_findAll];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.people.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

	FamousPerson *person = self.people[indexPath.row];
	cell.textLabel.text = person.name;

	return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Return NO if you do not want the specified item to be editable.
	return YES;
}

#pragma mark - system events

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([segue.identifier isEqualToString:@"showDetail"])
	{
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		FamousPerson *person = self.people[indexPath.row];
		[[segue destinationViewController] setFamousPerson:person];
		[[segue destinationViewController] setRowNumber: indexPath.row];
	}
}


@end
