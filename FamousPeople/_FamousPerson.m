// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FamousPerson.m instead.

#import "_FamousPerson.h"

const struct FamousPersonAttributes FamousPersonAttributes = {
	.birthCoordinates = @"birthCoordinates",
	.birthDate = @"birthDate",
	.birthLocation = @"birthLocation",
	.deathCoordinates = @"deathCoordinates",
	.deathDate = @"deathDate",
	.deathLocation = @"deathLocation",
	.gender = @"gender",
	.name = @"name",
};

const struct FamousPersonRelationships FamousPersonRelationships = {
};

const struct FamousPersonFetchedProperties FamousPersonFetchedProperties = {
};

@implementation FamousPersonID
@end

@implementation _FamousPerson

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FamousPerson" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FamousPerson";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FamousPerson" inManagedObjectContext:moc_];
}

- (FamousPersonID*)objectID {
	return (FamousPersonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic birthCoordinates;






@dynamic birthDate;






@dynamic birthLocation;






@dynamic deathCoordinates;






@dynamic deathDate;






@dynamic deathLocation;






@dynamic gender;






@dynamic name;











@end
