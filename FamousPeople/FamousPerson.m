//
//  FamousPerson.m
//  FamousPeople
//
//  Created by jbott on 8/25/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "FamousPerson.h"


@implementation FamousPerson

@dynamic name;
@dynamic gender;
@dynamic birthDate;
@dynamic birthLocation;
@dynamic birthCoordinates;
@dynamic deathDate;
@dynamic deathLocation;
@dynamic deathCoordinates;

@end
