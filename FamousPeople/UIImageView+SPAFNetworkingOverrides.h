#import <Foundation/Foundation.h>

@interface UIImageView (SPAFNetworkingOverrides)

- (void)setImageWithUrlString:(NSString *)urlString;

- (void)setImageWithUrlString:(NSString *)urlString shouldCache:(bool)cachingFlag;

- (void)setImageWithURL:(NSURL *)url
			shouldCache:(bool)cachingFlag;


- (void)setImageWithURL:(NSURL *)url
				success:(void ( ^ )(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
				failure:(void ( ^ )(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)image;
@end