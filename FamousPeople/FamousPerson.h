//
//  FamousPerson.h
//  FamousPeople
//
//  Created by jbott on 8/25/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface FamousPerson : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSString * birthDate;
@property (nonatomic, retain) NSString * birthLocation;
@property (nonatomic, retain) NSString * birthCoordinates;
@property (nonatomic, retain) NSString * deathDate;
@property (nonatomic, retain) NSString * deathLocation;
@property (nonatomic, retain) NSString * deathCoordinates;

@end
