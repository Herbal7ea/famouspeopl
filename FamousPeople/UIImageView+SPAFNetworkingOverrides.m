#import <AFNetworking/UIImageView+AFNetworking.h>
#import "UIImageView+SPAFNetworkingOverrides.h"


@implementation UIImageView (SPAFNetworkingOverrides)

- (void)setImageWithUrlString:(NSString *)urlString
{
	NSURL *url = [[NSURL alloc] initWithString:urlString];
	[self setImageWithURL:url shouldCache:true];
}

- (void)setImageWithUrlString:(NSString *)urlString shouldCache:(bool)cachingFlag
{
	NSURL *url = [[NSURL alloc] initWithString:urlString];
	[self setImageWithURL:url shouldCache:cachingFlag];
}

- (void)setImageWithURLString:(NSString *)urlString placeholderImage:(UIImage *)image
{
	NSURL *url = [NSURL URLWithString:urlString];
	[self setImageWithURL:url placeholderImage:image];
}

- (void)setImageWithURL:(NSURL *)url shouldCache:(bool)cachingFlag
{
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request addValue:@"image/*" forHTTPHeaderField:@"Accept"];

	if(!cachingFlag)
	{
		request.cachePolicy=NSURLRequestReloadIgnoringCacheData;
	}

	[self setImageWithURLRequest:request placeholderImage:nil success:nil failure:nil];
}

- (void)setImageWithURL:(NSURL *)url success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure
{
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	[request addValue:@"image/*" forHTTPHeaderField:@"Accept"];

	[self setImageWithURLRequest:request placeholderImage:nil success:success failure:failure];
}
@end