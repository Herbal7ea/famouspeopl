//
//  LocalHTTPServer.h
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "HTTPServer.h"


@interface LocalHTTPServer : HTTPServer

+ (instancetype)sharedInstance;

@end
