//
//  APIClient.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "APIClient.h"
#import "FamousPerson.h"


@implementation APIClient

+ (instancetype)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"http://localhost:12866"]];
    } );
    
    return _sharedInstance;
}

- (void)fetchMovies
{
    //race condition
    [self GET:@"FamousPeople.json" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
            
            [FamousPerson MR_importFromArray:responseObject inContext:localContext];
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog( @"FAILED: %@", error );
    }];
}

- (void)logPeople
{
	NSArray *people = [FamousPerson MR_findAll];

	NSLog( @"Number of People: %lu", (unsigned long)people.count );

	[people enumerateObjectsUsingBlock:^( FamousPerson *famousPerson, NSUInteger genreIndex, BOOL *genreStop ){
		NSLog( @"Genre: %@", famousPerson.name );
	}];
}

@end
