//
//  DetailViewController.m
//  Presenting Master-Detail Views with UISplitViewController
//
//  Created by Vandad NP on 02/07/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "DetailViewController.h"
#import "FamousPerson.h"
#import "UIImageView+SPAFNetworkingOverrides.h"

@interface DetailViewController ()

@property(strong, nonatomic) UIPopoverController *masterPopoverController;

@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *birthDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *deathDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthLocation;
@property (weak, nonatomic) IBOutlet UILabel *deathLocation;


@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self configureView];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
	return NO;
}

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
	barButtonItem.title = NSLocalizedString(@"People", @"People");
	[self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
	self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	self.masterPopoverController = nil;
}

#pragma mark - helper methods

- (void)configureView
{
	UIImage *defaultAvatarImage = [UIImage imageNamed:@"defaultUserAvatar.png"];

	if(self.famousPerson )
	{
		self.title 				 = _famousPerson.name;
		
		self.genderLabel.text 	 = _famousPerson.gender;
		self.birthDateLabel.text = _famousPerson.birthDate;
		self.birthLocation.text  = _famousPerson.birthLocation;
		self.deathDateLabel.text = _famousPerson.deathDate;
		self.deathLocation.text  = _famousPerson.deathLocation;

		NSString *imageUrlString = @"http://lorempixel.com/100/100/";
		NSString *modifiedImageUrlString = [NSString stringWithFormat:@"%@?row=%i", imageUrlString, _rowNumber];

		[self.photo setImageWithURLString:modifiedImageUrlString placeholderImage:defaultAvatarImage];
	}
}

- (void)setFamousPerson:(id)newFamousPerson
{
	if( _famousPerson != newFamousPerson )
	{
		_famousPerson = newFamousPerson;

		[self configureView];
	}

	if(self.masterPopoverController != nil)
		[self.masterPopoverController dismissPopoverAnimated:YES];
}

- (void)setRowNumber:(NSInteger)rowNumber
{
	_rowNumber = rowNumber;
}

@end
