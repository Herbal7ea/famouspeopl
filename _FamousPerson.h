// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FamousPerson.h instead.

#import <CoreData/CoreData.h>


extern const struct FamousPersonAttributes {
	__unsafe_unretained NSString *birthCoordinates;
	__unsafe_unretained NSString *birthDate;
	__unsafe_unretained NSString *birthLocation;
	__unsafe_unretained NSString *deathCoordinates;
	__unsafe_unretained NSString *deathDate;
	__unsafe_unretained NSString *deathLocation;
	__unsafe_unretained NSString *gender;
	__unsafe_unretained NSString *name;
} FamousPersonAttributes;

extern const struct FamousPersonRelationships {
} FamousPersonRelationships;

extern const struct FamousPersonFetchedProperties {
} FamousPersonFetchedProperties;











@interface FamousPersonID : NSManagedObjectID {}
@end

@interface _FamousPerson : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FamousPersonID*)objectID;





@property (nonatomic, strong) NSString* birthCoordinates;



//- (BOOL)validateBirthCoordinates:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* birthDate;



//- (BOOL)validateBirthDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* birthLocation;



//- (BOOL)validateBirthLocation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* deathCoordinates;



//- (BOOL)validateDeathCoordinates:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* deathDate;



//- (BOOL)validateDeathDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* deathLocation;



//- (BOOL)validateDeathLocation:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* gender;



//- (BOOL)validateGender:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






@end

@interface _FamousPerson (CoreDataGeneratedAccessors)

@end

@interface _FamousPerson (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBirthCoordinates;
- (void)setPrimitiveBirthCoordinates:(NSString*)value;




- (NSString*)primitiveBirthDate;
- (void)setPrimitiveBirthDate:(NSString*)value;




- (NSString*)primitiveBirthLocation;
- (void)setPrimitiveBirthLocation:(NSString*)value;




- (NSString*)primitiveDeathCoordinates;
- (void)setPrimitiveDeathCoordinates:(NSString*)value;




- (NSString*)primitiveDeathDate;
- (void)setPrimitiveDeathDate:(NSString*)value;




- (NSString*)primitiveDeathLocation;
- (void)setPrimitiveDeathLocation:(NSString*)value;




- (NSString*)primitiveGender;
- (void)setPrimitiveGender:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
